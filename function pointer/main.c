#include <stdio.h>
#include <stdlib.h>

#include "file1.h" // include file .h cua file1.c



int add(int a, int b) 
{
    return a+b;
} 

int sub(int a, int b)
{
    return a - b;
}

int calc(int a, int b, int (*func)(int,int))
{
    return (*func)(a,b);
}
  
int compare (const void * a, const void * b) 
{ 
  return ( *(int*)a - *(int*)b ); 
}


void m_print(int x)
{
    printf("m_print: Value = %d \n",x);
}

// đăng ký function được dùng trong file file1.c
// trong file1.c ta dùng hàm func thay cho hàm m_print
// hàm m_print được định nghĩa trong chương trình main
// Như vậy trong chương trình file1.c ta không cần include ngược file main.h
void register_function()
{
    func = m_print;
}


void main()
{
    // vd1: dùng con trỏ hàm func_ptr để thay cho con tên function add và plus
    int ch;
    int (*func_ptr[])(int,int) = {add, sub};
    ch = (*func_ptr[1])(2,3);
    // vd2: Dùng hàm calc với con trỏ hàm gán vào sau với mục đích clear trong khi code
    // hàm calc sẽ được gọi với mục đích tính toán, sau đó tên các hàm add, sub... được gọi như một tham số
    printf("Calc = %d \n",calc(2,3,add));


    // vd3: dùng con trỏ hàm trong 2 file khác nhau
    // đăng ký function của file1.c vào chương trình chính
    register_function();
    // dùng hàm trong file1.c 
    func_print(10);

    
    // vd4: quicksort sử dụng hàm compare
    int arr[] = {10, 5, 15, 12, 90, 80}; 
    int n = sizeof(arr)/sizeof(arr[0]), i;
    qsort (arr, n, sizeof(int), compare); 
    printf("mang: \n");
    for (i=0; i<n; i++) 
        printf ("%d ", arr[i]); 
    printf("\n");
}